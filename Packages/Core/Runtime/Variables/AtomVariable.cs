using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityAtoms {
    /// <summary>
    /// Generic base class for Variables. Inherits from `AtomBaseVariable&lt;T&gt;`.
    /// </summary>
    /// <typeparam name="T">The Variable value type.</typeparam>
    /// <typeparam name="P">IPair of type `T`.</typeparam>
    /// <typeparam name="E1">Event of type `AtomEvent&lt;T&gt;`.</typeparam>
    /// <typeparam name="E2">Event of type `AtomEvent&lt;T, T&gt;`.</typeparam>
    /// <typeparam name="F">Function of type `FunctionEvent&lt;T, T&gt;`.</typeparam>
    [EditorIcon("atom-icon-lush")]
    public abstract class AtomVariable<T, P, E1, E2, F> : AtomBaseVariable<T>, IGetEvent, ISetEvent
        where P : struct, IPair<T>
        where E1 : AtomEvent<T>
        where E2 : AtomEvent<P>
        where F : AtomFunction<T, T> {
        /// <summary>
        /// The Variable value as a property.
        /// </summary>
        /// <returns>Get or set the Variable's value.</returns>
        public override T Value {
            get {
                if (useXor) {
                    var result = PerformBaseXor(_value);
                    return result;
                }
                return _value;
                // if (name.StartsWith("TownLevelInfoOrganiser currentTownCount"))
                // Debug.Log("Getting value for" + name + " " + _value + " with xor:" + _xorSeed + " and result:" + result);
                //return _value; 
            }
            set {
                SetValue(value);
                // if (name.StartsWith("TownLevelInfoOrganiser currentTownCount"))
                // Debug.Log("Setting value for" + name + " " + value + "with xor:" + _xorSeed + " with _value:" + _value);
            }
        }

        public override string ValueString() {
            if (Value == null) {
                return "NULL";
            }
            return Value.ToString();
        }

        /// <summary>
        /// The initial value as a property.
        /// </summary>
        /// <returns>Get the Variable's initial value.</returns>
        public virtual T InitialValue {
            get {
                var result = _initialValue;
                return result;
            }
            set {
                _initialValue = value;
            }
        }

        /// <summary>
        /// The value the Variable had before its value got changed last time.
        /// </summary>
        /// <value>Get the Variable's old value.</value>
        public T OldValue {
            set {
                if (!useXor) {
                    _oldValue = value;
                } else {
                    _oldValue = PerformBaseXor(value);
                }
            }
            get {
                if (!useXor) {
                    return _oldValue;
                } else {
                    return PerformBaseXor(_oldValue);
                }
            }
        }

        /// <summary>
        /// Changed Event triggered when the Variable value gets changed.
        /// </summary>
        public E1 Changed;

        /// <summary>
        /// Changed with history Event triggered when the Variable value gets changed.
        /// </summary>
        public E2 ChangedWithHistory;


        public T PerformBaseXor(T value) {
            if (useXor) {
                if (_xorValue != null && _xorValue.Length == 16) {
                    return PerformXor(value);
                }
            }
            return value;
        }
        public virtual T PerformXor(T value) {
            return value;
        }

        public void ChangeXor() {
            var tmpValue = Value;
            var tmpOld = OldValue;
            if (!useXor) {
                _xorValue = null;
                _value = tmpValue;
                _oldValue = tmpOld;
                return;
            }
            if(_debug)
              Debug.Log("Changing xor for:" + name);
            if (_xorValue == null || _xorValue.Length != 16) {
                _xorValue = new byte[16];
            }
            if (xorGen == null) {
                xorGen = new System.Random();
            }
            if(_debug)
              Debug.Log("Old xor bytes:" + _xorValue.Length + " " + BitConverter.ToString(_xorValue));
            xorGen.NextBytes(_xorValue);
            if(_debug)
              Debug.Log("New xor bytes:" + _xorValue.Length + " " + BitConverter.ToString(_xorValue));
            _value = PerformXor(tmpValue);
            if(_debug) {
                if (tmpValue.ToString() == PerformXor(_value).ToString()) {
                    // Debug.Log("Xorded:"+tmpValue+" and got new _value of:"+_value+" which when xored back is:"+PerformXor(_value));
                } else {
                    // Debug.Log("Xorded:"+tmpValue+" and got new _value of:"+_value+" which when xored back is:"+PerformXor(_value)+ "which is different");
                }
            }
            _oldValue = PerformXor(tmpOld);
        }

        [SerializeField]
        private T _oldValue;

        /// <summary>
        /// The inital value of the Variable.
        /// </summary>
        [SerializeField]
        private T _initialValue = default(T);


        /// <summary>
        /// When setting the value of a Variable the new value will be piped through all the pre change transformers, which allows you to create custom logic and restriction on for example what values can be set for this Variable.
        /// </summary>
        /// <value>Get the list of pre change transformers.</value>
        public List<F> PreChangeTransformers {
            get => _preChangeTransformers;
            set {
                if (value == null) {
                    _preChangeTransformers.Clear();
                } else {
                    _preChangeTransformers = value;
                }
            }
        }

        [SerializeField]
        private List<F> _preChangeTransformers = new List<F>();

        protected abstract bool ValueEquals(T other);

        private void OnValidate() {
#if UNITY_EDITOR
            ChangeXor();
#endif
            InitialValue = RunPreChangeTransformers(InitialValue);
            Value = RunPreChangeTransformers(Value);
            if (_debug) {
                Debug.Log(name + " changed value to:" + Value, this);
            }
        }

        private void OnEnable() {
#if UNITY_EDITOR
            ChangeXor();
#endif
            OldValue = InitialValue;
            Value = InitialValue;
            if (_debug) {
                Debug.Log(name + " changed value to:" + Value, this);
            }

            if (Changed == null) return;
            Changed.Raise(Value);
        }

        /// <summary>
        /// Reset the Variable to its `_initialValue`.
        /// </summary>
        /// <param name="shouldTriggerEvents">Set to `true` if Events should be triggered on reset, otherwise `false`.</param>
        public override void Reset(bool shouldTriggerEvents = false) {
            if (_debug) {
                if (InitialValue == null) {
                    Debug.Log(name + " initial value is: null", this);
                } else {
                    Debug.Log(name + " initial value is:" + InitialValue.ToString(), this);
                }
            }
            OldValue = InitialValue;//_value;
            Value = InitialValue;
            Changed.ClearBuffer(InitialValue);
            if (_debug) {
                if (Value == null) { 
                    Debug.Log(name + " changed reset to: null", this);
                } else{
                    Debug.Log(name + " changed reset to:" + Value.ToString(), this);
                }
            }
            if (shouldTriggerEvents) {
                Changed.RaiseLastValue();
            }
        }

        /// <summary>
        /// Set the Variable value.
        /// </summary>
        /// <param name="newValue">The new value to set.</param>
        /// <returns>`true` if the value got changed, otherwise `false`.</returns>
        public bool SetValue(T newValue) {
            var preProcessedNewValue = RunPreChangeTransformers(newValue);

            if (_reraiseOnSameValue || !ValueEquals(preProcessedNewValue)) {
                OldValue = Value;
                if (useXor) {
                    _value = PerformBaseXor(preProcessedNewValue);
                    if (_debug) {
                        // Debug.Log("Changed xored _value to:" + _value, this);
                    }
                } else {
                    _value = preProcessedNewValue;
                }
                if (_debug) {
                    Debug.Log("Changed value to:" + Value, this);
                }

                if (Changed != null) {
                    if (_debug) {
                        Debug.Log("Raising new changed value of:" + Value, Changed);
                    }
                    Changed.Raise(Value);
                }
                if (ChangedWithHistory != null) {
                    // NOTE: Doing new P() here, even though it is cleaner, generates garbage.
                    var pair = default(P);
                    pair.Item1 = Value;
                    pair.Item2 = OldValue;
                    ChangedWithHistory.Raise(pair);
                }
                ChangeXor();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Set the Variable value.
        /// </summary>
        /// <param name="variable">The value to set provided from another Variable.</param>
        /// <returns>`true` if the value got changed, otherwise `false`.</returns>
        public bool SetValue(AtomVariable<T, P, E1, E2, F> variable) {
            return SetValue(variable.Value);
        }

        #region Observable

        /// <summary>
        /// Turn the Variable's change Event into an `IObservable&lt;T&gt;`. Makes the Variable's change Event compatible with for example UniRx.
        /// </summary>
        /// <returns>The Variable's change Event as an `IObservable&lt;T&gt;`.</returns>
        public IObservable<T> ObserveChange() {
            if (Changed == null) {
                throw new Exception("You must assign a Changed event in order to observe variable changes.");
            }

            return new ObservableEvent<T>(Changed.Register, Changed.Unregister);
        }

        /// <summary>
        /// Turn the Variable's change with history Event into an `IObservable&lt;T, T&gt;`. Makes the Variable's change with history Event compatible with for example UniRx.
        /// </summary>
        /// <returns>The Variable's change Event as an `IObservable&lt;T, T&gt;`.</returns>
        public IObservable<P> ObserveChangeWithHistory() {
            if (ChangedWithHistory == null) {
                throw new Exception("You must assign a ChangedWithHistory event in order to observe variable changes.");
            }

            return new ObservableEvent<P>(ChangedWithHistory.Register, ChangedWithHistory.Unregister);
        }
        #endregion // Observable

        private T RunPreChangeTransformers(T value) {
            if (_preChangeTransformers.Count <= 0) {
                return value;
            }

            var preProcessedValue = value;
            for (var i = 0; i < _preChangeTransformers.Count; ++i) {
                var Transformer = _preChangeTransformers[i];
                if (Transformer != null) {
                    preProcessedValue = Transformer.Call(preProcessedValue);
                }
            }


            return preProcessedValue;
        }

        /// <summary>
        /// Get event by type.
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <returns>The event.</returns>
        public E GetEvent<E>() where E : AtomEventBase {
            if (typeof(E) == typeof(E1))
                return (Changed as E);
            if (typeof(E) == typeof(E2))
                return (ChangedWithHistory as E);

            throw new Exception($"Event type {typeof(E)} not supported! Use {typeof(E1)} or {typeof(E2)}.");
        }

        /// <summary>
        /// Set event by type.
        /// </summary>
        /// <param name="e">The new event value.</param>
        /// <typeparam name="E"></typeparam>
        public void SetEvent<E>(E e) where E : AtomEventBase {
            if (typeof(E) == typeof(E1)) {
                Changed = (e as E1);
                return;
            }
            if (typeof(E) == typeof(E2)) {
                ChangedWithHistory = (e as E2);
                return;
            }

            throw new Exception($"Event type {typeof(E)} not supported! Use {typeof(E1)} or {typeof(E2)}.");
        }


    }
}
