using UnityEngine;
using UnityEngine.Events;

namespace UnityAtoms
{
    /// <summary>
    /// Generic base class for Listeners using Event Reference. Inherits from `AtomListener&lt;T, E, UER&gt;` and implements `IAtomListener&lt;T&gt;`.
    /// </summary>
    /// <typeparam name="T">The type that we are listening for.</typeparam>
    /// <typeparam name="E">Event of type `T`.</typeparam>
    /// <typeparam name="ER">Event Reference of type `T`.</typeparam>
    /// <typeparam name="UER">UnityEvent of type `T`.</typeparam>
    [EditorIcon("atom-icon-orange")]
    public abstract class AtomEventReferenceListener<T, E, ER, UER> : AtomBaseListener<T, E, UER>, IAtomListener<T>
        where E : AtomEvent<T>
        where ER : AtomBaseEventReference, IGetEvent, ISetEvent, new()
        where UER : UnityEvent<T>
    {
        /// <summary>
        /// The Event we are listening for as a property.
        /// </summary>
        /// <value>The Event Reference of type `ER`.</value>
        public override E Event {
            get {
                return _eventReference.GetEvent<E>();
            }
            set {
                var oldEvent = _eventReference.GetEvent<E>();
                if (oldEvent != null) Event.UnregisterListener(this);
                _eventReference.SetEvent<E>(value);
                if (value != null) value.RegisterListener(this, _replayEventBufferOnRegister);
            }
        }

        /// <summary>
        /// The Event Reference that we are listening to.
        /// </summary>
        [SerializeField]
        private ER _eventReference = new ER();//default(ER);
    }
}
